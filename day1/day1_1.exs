File.stream!("input.txt")
|> Enum.map(&String.trim/1)
|> Enum.reduce(0, fn x, acc ->
  y =
    x
    |> String.trim()
    |> String.to_integer()
    |> div(3)
    |> floor()

  acc + (y - 2)
end)
|> IO.inspect()
