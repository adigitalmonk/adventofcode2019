# defmodule Array do
#     def new, do: :array.new()

#     def add(arr, index, data), do: :array.set(index, value, arr)

#     def get(arr, index), do: :array.get(index, arr)
# end

defmodule WireTracer do

    def handle_instruction({ "U", 0}, { x, y }, arr), do: { {x, y} , arr }
    def handle_instruction({ "U", distance }, { x, y }, arr) do
        new_point = { x, y + 1}
        handle_instruction({ "U", distance - 1 }, new_point, [ new_point | arr ])
    end

    def handle_instruction({ "D", 0}, { x, y }, arr), do: { {x, y} , arr }
    def handle_instruction({ "D", distance }, { x, y }, arr) do
        new_point = { x, y - 1}
        handle_instruction({ "D", distance - 1 }, new_point, [ new_point | arr ])
    end

    def handle_instruction({ "R", 0}, { x, y }, arr), do: { {x, y} , arr }
    def handle_instruction({ "R", distance }, { x, y }, arr) do
        new_point = { x + 1, y}
        handle_instruction({ "R", distance - 1 }, new_point, [ new_point | arr ])
    end

    def handle_instruction({ "L", 0}, { x, y }, arr), do: { {x, y} , arr }
    def handle_instruction({ "L", distance }, { x, y }, arr) do
        new_point = { x - 1, y}
        handle_instruction({ "L", distance - 1 }, new_point, [ new_point | arr ])
    end

    def manhatten({ x, y }) do
        abs(x) + abs(y)
    end

    def parse_instructions(raw_data) do
        [ setA, setB ] = 
            raw_data
            |> String.split()
            |> Enum.map(fn set -> 
                String.split(set, ",")
                |> Enum.map(fn << direction::binary-size(1), rest::binary >> -> 
                    { direction, String.to_integer(rest) }
                end)
            end)
            |> Enum.map(fn instruction_set -> 
                {{_,_}, arr} =
                    instruction_set
                    |> Enum.reduce({ { 0, 0 } , [] }, fn instruction, { { x, y }, arr } -> 
                        handle_instruction(instruction, {x, y}, arr)
                    end)

                Enum.reverse(arr)
            end)

        # TODO: This finds an answer that is too low
        # and is slow as heck
        [
            Enum.find_index(setA, fn {xA, yA} -> 
                Enum.find(setB, fn {xB, yB} -> 
                    xA == xB && yA == yB
                end)
                |> IO.inspect
            end),
            Enum.find_index(setB, fn {xA, yA} -> 
                Enum.find(setA, fn {xB, yB} -> 
                    xA == xB && yA == yB
                end)
            end)
        ]
    end

    def run(filename) do
        File.read!(filename)
        |> parse_instructions()
    end
end


:timer.tc(WireTracer, :run, ["input.txt"])
|> IO.inspect()
